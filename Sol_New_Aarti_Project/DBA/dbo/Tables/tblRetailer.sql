﻿CREATE TABLE [dbo].[tblRetailer] (
    [RetailerId]       NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [RetailerName]     VARCHAR (50)  NULL,
    [RetailerAddress]  VARCHAR (50)  NULL,
    [RetailerLocation] VARCHAR (200) NULL,
    [RetailerPincode]  VARCHAR (50)  NULL,
    [MobileNumber]     VARCHAR (10)  NULL,
    [EmailId]          VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([RetailerId] ASC)
);

