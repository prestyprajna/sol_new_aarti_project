﻿CREATE TABLE [dbo].[tblOption] (
    [OptionId]   NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [OptionName] VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([OptionId] ASC)
);

