﻿CREATE TABLE [dbo].[tblProduct] (
    [ProductId]          NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [ProductCode]        VARCHAR (50) NULL,
    [ProductName]        VARCHAR (50) NULL,
    [ProductDescription] VARCHAR (50) NULL,
    [ProductQuantity]    INT          NULL,
    [ProductCost]        NUMERIC (18) NULL,
    [ProductStock]       NUMERIC (18) NULL,
    PRIMARY KEY CLUSTERED ([ProductId] ASC)
);

