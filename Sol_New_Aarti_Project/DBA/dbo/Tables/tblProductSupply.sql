﻿CREATE TABLE [dbo].[tblProductSupply] (
    [RetailerId]   NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [RetailerName] VARCHAR (50)  NULL,
    [Quantity]     VARCHAR (50)  NULL,
    [OptionId]     NUMERIC (18)  NULL,
    [EmailId]      VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([RetailerId] ASC),
    FOREIGN KEY ([OptionId]) REFERENCES [dbo].[tblOption] ([OptionId])
);

