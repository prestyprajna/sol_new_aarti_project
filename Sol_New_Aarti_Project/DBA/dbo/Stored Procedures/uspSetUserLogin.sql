﻿CREATE PROCEDURE uspSetUserLogin
(
	@Command VARCHAR(MAX),
	--@FirstName VARCHAR(50),
	--@LastName VARCHAR(50),
	@UserName VARCHAR(50),
	@Password VARCHAR(50),
	@UserId NUMERIC(18,0) 
)
AS
	BEGIN

	DECLARE @ErrorMessage VARCHAR(MAX)	
			
			IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION		

				BEGIN TRY

					INSERT INTO tblUserLogin
					(
						UserName,
						Password,
						UserId

					)
					VALUES 
					(
						@UserName,
						@Password,
						@UserId
					)

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					SET @ErrorMessage=ERROR_MESSAGE()
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)
				END CATCH	

			END		

	END
