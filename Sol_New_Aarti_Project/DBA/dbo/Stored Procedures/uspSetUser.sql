﻿CREATE PROCEDURE uspSetUser
(
	@Command VARCHAR(MAX),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@UserName VARCHAR(50),
	@Password VARCHAR(50),
	@UserId NUMERIC(18,0) ,
	@Status INT OUT ,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	DECLARE @ErrorMessage VARCHAR(MAX)		
			
			IF @Command='INSERT'
			BEGIN

				BEGIN TRANSACTION

				BEGIN TRY

					INSERT INTO tblUser
					(
						FirstName,
						LastName
					)
					VALUES 
					(
						@FirstName,
						@LastName
					)

					SET @UserId=@@IDENTITY

					EXEC uspSetUserLogin @Command,@UserName,@Password,@UserId

					SET @Status=1
					SET @Message='INSERT SUCCESSFULL'

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='INSERT EXCEPTION'
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)
				END CATCH	

			END		

	END
