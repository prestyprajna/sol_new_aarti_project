﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUpPage.aspx.cs" Inherits="Sol_New_Aarti_Project.SignUpPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style type="text/css">
        table{
            margin:auto;
            padding:200px;          

        }

       .textBoxStyle{
           width:400px;
           height:60px;
           border-radius:10px;
       }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>
            <tr>                
                <td>                
                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName" CssClass="textBoxStyle w3-white"></asp:TextBox>                   
                </td>
            </tr>

            <tr>                
                <td >
                    <asp:TextBox ID="txtLastName" runat="server" placeholder="LastName" CssClass="textBoxStyle  w3-white"></asp:TextBox>
                </td>
            </tr>

            <tr>                
                <td >                
                    <asp:TextBox ID="txtUserName" runat="server" placeholder="UserName" CssClass="textBoxStyle w3-white"></asp:TextBox>                   
                </td>
            </tr>

            <tr>                
                <td >
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" CssClass="textBoxStyle  w3-white" TextMode="Password"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td >
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="w3-button w3-hover-green"   />
                </td>                
            </tr>
            
        </table>
    
    </div>
    </form>
</body>
</html>
