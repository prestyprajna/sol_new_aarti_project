﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="Sol_New_Aarti_Project.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style type="text/css">
        table{
            margin:auto;
            padding:200px;          

        }

       .textBoxStyle{
           width:400px;
           height:60px;
           border-radius:10px;
       }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>
            <tr>                
                <td colspan="2">                
                    <asp:TextBox ID="txtUserName" runat="server" placeholder="UserName" CssClass="textBoxStyle w3-white"></asp:TextBox>                   
                </td>
            </tr>

            <tr>                
                <td colspan="2">
                    <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" CssClass="textBoxStyle  w3-white" TextMode="Password"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td >
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="w3-button w3-hover-green"   />
                </td>
                <td>
                    <asp:LinkButton ID="lnkForgotPassword" runat="server" Text="Forgot Password" ForeColor="Red"></asp:LinkButton>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <asp:Button ID="btnRegistration" runat="server" Text="Click here to Register User" CssClass="w3-button w3-green" PostBackUrl="~/SignUpPage.aspx"   />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
