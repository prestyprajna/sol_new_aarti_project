﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="Sol_New_Aarti_Project.View.Pages.Product" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        table{
            margin:auto;
            padding:100px;          

        }

       .textBoxStyle{
           width:400px;
           height:50px;
           border-radius:10px;
       }

    </style>

    <table>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtProductCode" runat="server" placeholder="Product Code" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtProductName" runat="server" placeholder="Product Name" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>   
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtProductDescription" runat="server" placeholder="Product Description" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>            
            <tr>
                <td colspan="2">                    
                    <asp:CheckBoxList ID="chkProductQuantity" runat="server">                        
                        <asp:ListItem Value="1" Text="OPTION1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="OPTION2"></asp:ListItem>
                        <asp:ListItem Value="3" Text="OPTION3"></asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtProductCost" runat="server" placeholder="Product Cost" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtProductStock" runat="server" placeholder="Product Stock" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="w3-button w3-green" />
                </td>
                <td>
                    <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="w3-button w3-green" />
                </td>
            </tr>
        </table>

</asp:Content>
